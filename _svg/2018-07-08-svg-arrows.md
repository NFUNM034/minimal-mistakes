---
title:  "SVG箭头动图"
---

## 会兜圈圈的箭头 

这是一个会兜圈圈的箭头。

<html>
	<head>
		<meta charset="UTF-8">
		<title></title>
	</head>
	<body>
        <svg width="520" height="520" xmlns="http://www.w3.org/2000/svg">
          <g> 
              <path transform="rotate(-179.6085205078125 303.8800048828125,154.470703125) " id="svg_1" d="m271.379992,154.3735l32.499998,-40.402796l32.499998,40.402796l-16.25,0l0,40.597207l-32.499996,0l0,-40.597207l-16.25,0z" stroke="#0351a0" fill="#003f7f"/>
              <animateTransform attributeName="transform" begin="0s" dur="4s" type="rotate" from="0 180 210" to="360 160 160" repeatCount="indefinite"/>
         </g>
</svg>
	</body>
</html>

箭头会动的关键代码：
```
<animateTransform attributeName="transform" begin="0s" dur="4s" type="rotate" from="0 180 210" to="360 160 160" repeatCount="indefinite"/>
```

**代码释义**
- animateTransform是动画上一个目标元素变换属性，从而使动画控制平移，缩放，旋转或倾斜
- by="相对偏移值"
- from="起始值"
- to="结束值"
- type="类型的转换其值是随时间变化。可以是 'translate', 'scale', 'rotate', 'skewX', 'skewY'"

这个箭头以“rotate”类型，经4s的时间运动一次，并不断重复这一动作。

