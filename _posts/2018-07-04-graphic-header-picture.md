---
title:  "设置页头图片"
modified: 2018-07-04 T16:03:49-04:00
categories: 
  - 平面设计
tags:
  - 笔记
header:
  overlay_image: /images/pink-book-unsplash.jpg
  caption: "Photo credit: **Unsplash**"
  teaser: /images/pink-book-unsplash.jpg
sidebar:
  nav: "docs"
---

# 平面设计之图片的设置

{% include base_path %}

{% include toc title="目录" %}

{% include gallery caption="This is a sample gallery with **Markdown support**." %}

> 实用性与审美之间的优秀平衡能够取悦眼睛与大脑，即，美感能够增强可用性。

所以，“美”对于一个网页来说并不是无用的。在平面设计中，图片和文字要巧妙地安排以实现“1+1=3”的效果，要让用户感到“美”。

## 设置首页页头图片

首先为设置页头图片做准备。
把图片放在images文件夹里（没有的话就新建一个）。<br>
为大家提供一个图片网站 [unsplash](https://unsplash.com/)

index.html里代码应如下:
```
---
layout: home
author_profile: true
permalink: /
header:
  overlay_color: "#FFA6AC"
  overlay_image: /images/toa-heftiba-462392-unsplash.jpg
  caption: "Photo credit: [**Unsplash**](https://unsplash.com)"
#  cta_label: "<i class='fa fa-download'></i> Install Now"
#  cta_url: "/docs/quick-start-guide/"
  caption:
excerpt: '输入你想输入的话'

feature_row:
  - image_path: /images/unsplash-gallery-image-4.jpg
    alt: "customizable"
    title: "Super Customizable"
    excerpt: "Everything from the menus, sidebars, comments, and more can be configured or set with YAML Front Matter."
    url: "/docs/configuration/"
    btn_label: "Learn More"
  - image_path: mm-responsive-feature.png
    alt: "fully responsive"
    title: "Responsive Layouts"
    excerpt: "Built on HTML5 + CSS3. All layouts are fully responsive with helpers to augment your content."
    url: "/docs/layouts/"
    btn_label: "Learn More"
  - image_path: mm-free-feature.png
    alt: "100% free"
    title: "100% Free"
    excerpt: "Free to use however you want under the MIT License. Clone it, fork it, customize it, whatever!"
    url: "/docs/license/"
    btn_label: "Learn More"
github:
  - excerpt: '{::nomarkdown}<iframe style="display: inline-block;" src="https://ghbtns.com/github-btn.html?user=mmistakes&repo=minimal-mistakes&type=star&count=true&size=large" frameborder="0" scrolling="0" width="160px" height="30px"></iframe> <iframe style="display: inline-block;" src="https://ghbtns.com/github-btn.html?user=mmistakes&repo=minimal-mistakes&type=fork&count=true&size=large" frameborder="0" scrolling="0" width="158px" height="30px"></iframe>{:/nomarkdown}'
intro:
  - excerpt: 'Get notified when I add new stuff &nbsp; [<i class="fa fa-twitter"></i> @mmistakes](https://twitter.com/mmistakes){: .btn .btn--twitter}'
---

{% include feature_row id="intro" type="center" %}

{% include feature_row %}
```


## 设置文章页头图片

代码如下：
```
excerpt: "输入你想要的内容"
header:
  overlay_image: /images/snow.jpg
  caption: "Photo credit: [**Unsplash**](https://unsplash.com)"
  cta_label: "More Info"
  cta_url: "https://unsplash.com"
```
效果如下图：
![页头效果图一]({{ site.url }}{{ site.baseurl }}/images/header-example-1.png)

也可以删改一些代码：
```
excerpt: "输入你想要的内容"
header:
  overlay_image: /images/snow.jpg
  caption: "Photo credit: **Unsplash**"
```
效果如下图：
![页头效果图二]({{ site.url }}{{ site.baseurl }}/images/header-example-2.png)

### 实践心得
通过实践，我注意到了以下的问题：
- 图片要统一放在images文件夹里。
- 图片命名要正确，使用无意义的英文、中文或数字等都是不正确的写法。
- 插入图片，格式路径要正确，不然就无法显示图片。
- 页头不插入图片也可以使用纯色的背景，一样会有自己的风格。