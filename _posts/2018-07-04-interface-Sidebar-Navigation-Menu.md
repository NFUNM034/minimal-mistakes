---
title:  "导航栏：快速准确的查找"
modified: 2018-07-04 T16:03:49-04:00
categories: 
  - 界面设计
tags:
  - 笔记
header:
  overlay_image: /images/pineapple-unsplash.jpg
  caption: "Photo credit: **Unsplash**"
  teaser: /images/pineapple-unsplash.jpg
sidebar:
  nav: "docs"
---

# 界面设计之导航栏

{% include base_path %}

{% include toc title="目录" %}

{% include gallery caption="This is a sample gallery with **Markdown support**." %}


当网站内有大量的文章，想要快速找到某一篇文章或者是文章中某一个关键词并不容易，这时一些界面设计就发挥了很大的作用。

## 页头导航栏

页头导航栏放置着网站的外链内链，通过页头导航栏，能够基本了解该网站的几大主要内容以及通过点击实现向站内站外的跳转。

![页头导航栏]({{ site.url }}{{ site.baseurl }}/images/header-navigation.png)

### 制作方法

在_data/navigation.yml输入代码：
```
main:
  - title: "简书"
    url: https://www.jianshu.com/u/a156d462444b
   
  - title: "SVG制作"
    url: /svg/
  
  - title: "界面设计"
    url: /design/#界面设计
  
  - title: "平面设计"
    url: /design/#平面设计
  
  - title: "关于我"
    url: /about/
```
**要注意格式！**

接下来要做的就是实现导航栏站内的跳转——**设置路径**

- 新建_pages文件夹，在这里放置导航栏生成的页面。
![_pages]({{ site.url }}{{ site.baseurl }}/images/pages.png)

- 新建about.md文件，根据自己的情况自行创作内容。
![about.md]({{ site.url }}{{ site.baseurl }}/images/about.png)

- 新建catagory-archive.md文件，设置“界面设计”“平面设计”路径。
![catagory-archive.md]({{ site.url }}{{ site.baseurl }}/images/catagory-archive.png)
- 新建design-archive.md文件。
![design-archive.md]({{ site.url }}{{ site.baseurl }}/images/design-archive.png)
- 新建svg-archive.md文件，设置出路径为/svg/的页面。
![svg-archive.md]({{ site.url }}{{ site.baseurl }}/images/svg-archive.png)

确保以下代码在config.yml中正确：
```
category_archive:
  type: liquid
  path: /design/
tag_archive:
  type: liquid
  path: /tags/
```

- 在config.yml输入以下代码：
``` 
# Collections
collections:
  svg:
    output: true
    permalink: /:collection/:path/

  # _svg
  - scope:
      path: ""
      type: svg
    values:
      layout: single
      author_profile: true
      share: true
      comments: true
```

完成好以上步骤，就做好页头导航栏了。（如有错误，欢迎指出）


## 侧边导航栏

做好页头导航，侧边栏就很好做了。

![侧边导航栏]({{ site.url }}{{ site.baseurl }}/images/sidebar.png)

### 制作方法

在_data/navigation.yml输入以下格式代码：
```
docs:
  - title: SVG制作
    children:
      - title: "SVG的第一篇文章"
        url: http://nfunm034.gitee.io/minimal-mistakes/svg/2018-07-06-svg-01/
      - title: "SVG的第二篇文章"
        url: http://nfunm034.gitee.io/minimal-mistakes/svg/2018-07-07-svg-02/
      - title: "SVG的第三篇文章"
        url: http://nfunm034.gitee.io/minimal-mistakes/svg/2018-07-08-svg-03/

  - title: 平面设计
    children:
      - title: "背景与字体颜色的搭配"
        url: http://nfunm034.gitee.io/minimal-mistakes/%E5%B9%B3%E9%9D%A2%E8%AE%BE%E8%AE%A1/graphic-color/
      - title: "平面设计的第二篇笔记"
        url: http://nfunm034.gitee.io/minimal-mistakes/%E5%B9%B3%E9%9D%A2%E8%AE%BE%E8%AE%A1/graphic-02/
      - title: "平面设计的第三篇笔记"
        url: http://nfunm034.gitee.io/minimal-mistakes/%E5%B9%B3%E9%9D%A2%E8%AE%BE%E8%AE%A1/graphic-03/
  
  - title: 界面设计
    children:
      - title: "页面加入侧边栏导航"
        url: http://nfunm034.gitee.io/minimal-mistakes/%E7%95%8C%E9%9D%A2%E8%AE%BE%E8%AE%A1/interface-Sidebar-Navigation-Menu/
      - title: "界面设计的第二篇笔记"
        url: http://nfunm034.gitee.io/minimal-mistakes/%E7%95%8C%E9%9D%A2%E8%AE%BE%E8%AE%A1/interface-02/
      - title: "界面设计的第三篇笔记"
        url: http://nfunm034.gitee.io/minimal-mistakes/%E7%95%8C%E9%9D%A2%E8%AE%BE%E8%AE%A1/interface-03/
```

在你想要增加侧边栏的文章内的YAML Front Matter输入：
```
sidebar:
  nav: "docs"
```
看不懂上面的可以看这里：
![sidebar]({{ site.url }}{{ site.baseurl }}/images/sidebar-example.png)

在_config.yml中对应位置输入代码：
```
defaults:
  # _docs
  - scope:
      path: ""
      type: docs
    values:
      sidebar:
        nav: "docs"
```


## 页头导航栏搜索

在config.yml中修改为以下代码：
```
search                   :   true # true, false (default)
```

**现在，你就可以快速准确地查找文章了。**

### 实践心得

不得不说，导航栏是网页一个重要组成部分。<br>
在制作导航栏的过程中，很容易出现错误，例如：代码格式不正确，使导航无法实现跳转；因文章重命名，url地址更改而使路径更改，导致404问题等等。<br>
细节的问题其实是一个大问题。