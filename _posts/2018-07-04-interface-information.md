---
title:  "个人信息的编辑"
modified: 2018-07-04 T16:03:49-04:00
categories: 
  - 界面设计
tags:
  - 笔记
header:
  overlay_image: /images/pineapple-unsplash.jpg
  caption: "Photo credit: **Unsplash**"
  teaser: /images/pineapple-unsplash.jpg
sidebar:
  nav: "docs"
---

# 界面设计之编辑个人信息

{% include base_path %}

{% include toc title="目录" %}

{% include gallery caption="This is a sample gallery with **Markdown support**." %}


拿到别人网页的模板后，第一件事就是要改个人信息。

Site Settings处需要修改的是整个网页框架的建设的信息。
![个人信息编辑]({{ site.url }}{{ site.baseurl }}/images/information.png)

author处需要修改的是侧边栏个人信息。
![个人信息编辑]({{ site.url }}{{ site.baseurl }}/images/information-1.png)
![个人信息编辑]({{ site.url }}{{ site.baseurl }}/images/information-2.png)


