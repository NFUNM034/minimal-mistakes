---
title:  "字体的设置"
modified: 2018-07-06 T16:03:49-04:00
categories: 
  - 平面设计
tags:
  - 笔记
header:
  overlay_image: /images/pink-book-unsplash.jpg
  caption: "Photo credit: **Unsplash**"
  teaser: /images/pink-book-unsplash.jpg
sidebar:
  nav: "docs"
---

# 平面设计之字体

{% include base_path %}

{% include toc title="目录" %}

{% include gallery caption="This is a sample gallery with **Markdown support**." %}


## 设置中文字形

在skins中新建一个自己的皮肤，我把这个皮肤命名为_aqua_yuhui.scss。可以在这个皮肤里修改自己喜欢的颜色和字体，应用到网页中。<br>
但要在_config.yml中修改以下地方：
```
minimal_mistakes_skin    : "aqua_yuhui" # "air", "aqua", "contrast", "dark", "dirt", "neon", "mint", "plum", "sunrise"
```
然后网页就会自动应用自己的皮肤。

在_aqua_yuhui.scss中输入代码：
```
/* system typefaces 使用中文字形 */
$serif: DFKai-SB,Kai,Kaiti SC,KaiTi,BiauKai,\\6977\4F53,\\6977\4F53_GB2312,Songti SC,Georgia, Times, serif !default;
$sans-serif: "Helvetica Neue",YouYuan,\\5E7C\5706,幼圆,"Segoe UI",-apple-system, BlinkMacSystemFont, "Roboto", "Segoe UI",
  "Helvetica Neue", "Lucida Grande", Arial, sans-serif !default;
$monospace: Monaco, Consolas, "Lucida Console", monospace !default;
```
就成功使用了自己设置的中文字形。
也可以通过改变字形顺序，改变字体。

## 设置衬线字体和无衬线体

> 平面设计的惯例是，内文（body texts）用衬线字体，而标题（heading）用无衬线体。但也会因情况及需要做调整。<br>
像投影片及科学海报，若内文字多的话常沿用标题无衬线，内文用衬线的惯例，但也有内文字不多且是列表式的内容，也有只用无衬线体的情况。<br>

**设置标题为无衬线体，内文为衬线字体很简单。**<br>

在_sass/minimal-mistakes/variables.scss中有以下设定：<br>
$global-font-family: $sans-serif !default;<br>
$header-font-family: $sans-serif !default;<br>
$caption-font-family: $serif !default;<br>

方法一：<br>
在variables.scss中将$global-font-family 设为 $serif，因为base.scss中body使用的是$global-font-family，就是说原本的正文使用的是$global-font-family。
![设置衬线字体和无衬线字体]({{ site.url }}{{ site.baseurl }}/images/serif-change.png)

方法二：
- 在base.scss里把body控制的全局的font-family: $global-font-family;去除； 
- 给p增加font-family: $caption-font-family；
![设置衬线字体和无衬线字体]({{ site.url }}{{ site.baseurl }}/images/serif-change-1.png)


小知识：
> 网页设计中的英文字体，一般有以下五类：
- serif（衬线）
- sans-serif（无衬线）
- monospace（等宽）
- fantasy（梦幻）
- cuisive（花体）

### 实践心得

- 网页皮肤什么的，都可以按照自己的想法来设计。
- 设计网页并不是随心所欲的，完全按照自己喜欢的方式或格式去做是不行的，它还是有一个框架，有惯例的。