---
title:  "界面的优化：添加小细节"
modified: 2018-07-04 T16:03:49-04:00
categories: 
  - 界面设计
tags:
  - 笔记
header:
  overlay_image: /images/pineapple-unsplash.jpg
  caption: "Photo credit: **Unsplash**"
  teaser: /images/pineapple-unsplash.jpg
sidebar:
  nav: "docs"
---

# 界面设计之优化

{% include base_path %}

{% include toc title="目录" %}

{% include gallery caption="This is a sample gallery with **Markdown support**." %}

## 添加面包屑导航

网站页面很多，用户可能出现在任何页面上，想要简单快速地回到前面的页面，面包屑导航(breadcrumbs)就发挥作用了。

### 面包屑导航添加方法
修改代码的位置如下：
![面包屑导航]({{ site.url }}{{ site.baseurl }}/images/breadcrumbs-change.png)
修改后，面包屑导航就会出现。

可是有一个尚未解决的问题，在平面设计与界面设计的文章里，面包屑导航出现乱码的情况，但是能够跳转成功。
![面包屑导航]({{ site.url }}{{ site.baseurl }}/images/breadcrumbs-messy-code.png)

在svg的文章里，面包屑导航却是正确显示出来了。
![面包屑导航]({{ site.url }}{{ site.baseurl }}/images/breadcrumbs-normal.png)

## 网站首页文章篇数的显示

当文章数量超过设定最多显示文章数量时，我们的文章就无法全部显示在首页。

### 解决方法
方法一：
- 在_config.yml中可随意修改文章显示的数量，这样在首页就能最多显示9篇文章了。
![首页文章显示]({{ site.url }}{{ site.baseurl }}/images/amount-of-posts-to-show.png)

方法二：
- 为首页文章设置分页。删除index.html中以下的内容。
![首页文章显示]({{ site.url }}{{ site.baseurl }}/images/pagnate-1.png)
![首页文章显示]({{ site.url }}{{ site.baseurl }}/images/pagnate-2.png)
因为我是跟着一个模板的步骤做的，这个方法在我这里有用，但可能在你那里会没有用。

## 添加文章推荐的图片
![添加图片]({{ site.url }}{{ site.baseurl }}/images/teaser-picture.png)
添加框内的图片，让你的网站看起来更丰富。

### 方法
- 在文章内YAML Front Matter里输入代码：
```
teaser: /images/your-picture.jpg
```


### 实践心得

作为《网页设计与制作》这门课程的初学者，要制作一个完美的网页是很难的，我们只好先做出一个大概的框架，做出一个网页最重要以及必须的部分，再去增加一些可以优化网页的东西。