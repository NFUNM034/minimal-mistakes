---
title:  "背景与字体颜色的搭配"
modified: 2018-07-06 T16:03:49-04:00
categories: 
  - 平面设计
tags:
  - 笔记
header:
  overlay_image: /images/pink-book-unsplash.jpg
  caption: "Photo credit: **Unsplash**"
  teaser: /images/pink-book-unsplash.jpg
sidebar:
  nav: "docs"
---

# 平面设计之搭配

{% include base_path %}

{% include toc title="目录" %}

{% include gallery caption="This is a sample gallery with **Markdown support**." %}


人类是视觉动物，良好的颜色搭配能为网页加分。

## 对比度

并不是所有颜色都是等价的。颜色搭配讲究[**对比度**](https://baike.baidu.com/item/对比度/1196082?fr=aladdin)。<br>
**那么如何为背景和字体挑选合适的颜色？**<br>

网页的背景颜色应选择较为舒服的颜色，切记不要选择荧光色！！！<br>
字体的颜色也要注意和背景的搭配。像浅色背景搭配浅色字体或者深色背景搭配深色字体就要避免了，无论你的内容有多丰富有趣，如果要让读者辛苦地去猜你究竟写了些什么，那就真的不用指望别人会去读你的东西。

以下是背景和字体的一些颜色搭配的小技巧。
- 颜色色差大会比较显眼
- 暖色调的搭配会让人觉得温暖可爱
- 冷色调的搭配会让人觉得宁静舒服
- 正式严肃的场合适合黑色灰色白色等较为冷的颜色
- 最好避免互补色，如红色和绿色，搭配起来会不好看

选择颜色可以使用[Adobe Color CC](https://color.adobe.com/zh/create/color-wheel/)
![Adobe Color CC_picture1.png]({{ site.url }}{{ site.baseurl }}/images/Adobe-Color-CC-picture-1.png)
![Adobe Color CC_picture2.png]({{ site.url }}{{ site.baseurl }}/images/Adobe-Color-CC-picture-2.png)

试着用灰阶打印检查。
![print.png]({{ site.url }}{{ site.baseurl }}/images/print.png)

若字体能够清晰看到，则对比度足够。

### 实践心得

本次实践注意到了对比度的问题，知道了如何用工具客观查看对比度。