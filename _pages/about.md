---
title: "关于我"
permalink: /about/
date: 2018-07-06T21:38:52+08:00
---

> 中山大学南方学院 文学与传媒学院 网络与新媒体专业学生，学习的课程：《网络与新媒体技术与协作》、《网页设计与制作》。

### 黄煜惠
- [简历](http://nfunm034.gitee.io/resume/)
- 正在学习、提升的技能：摄影、photoshop、Office软件 等等